<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    //
    public function index()
    {

        return view('dashboard');
    }

    public function addProduct(Request $request)
    {
        $this->validate($request, [
            'productname' => 'required',
            'productqtyinstock' => 'required',
            'priceperitem' => 'required'
        ]);

        if ($request->ajax()) {
            $productName = $request['productname'];
            $productQtyInStock = $request['productqtyinstock'];
            $pricePerItem = $request['priceperitem'];

            $addProduct = new Form();
            $addProduct->product_name = $productName;
            $addProduct->quantity_in_stock = $productQtyInStock;
            $addProduct->price_per_item = $pricePerItem;
            $addProduct->save();
            $data = Array('ajaxloginstatus' => true);
            return $data;
        } else {
            $data = Array('ajaxloginstatus' => false);
            return $data;
        }


    }

    public function fetchProducts()
    {

        $products = Form::orderBy('created_at', 'desc')->get();
        $total = 0;

        foreach ($products as $product) {
            $total += $product->quantity_in_stock * $product->price_per_item;
        }

        return ['products' => $products, 'total' => $total];
    }

    public function fetchSingleProduct(Request $request, $id)
    {
        $product = Form::find($id)->first();
        return $product;
    }


    public function postEditProduct(Request $request)
    {

        $productName = $request['productname'];
        $productQtyInStock = $request['productqty'];
        $pricePerItem = $request['productprice'];

        $addProduct = Form::find($request['productid']);
        $addProduct->product_name = $productName;
        $addProduct->quantity_in_stock = $productQtyInStock;
        $addProduct->price_per_item = $pricePerItem;
        if ($addProduct->update()) {
            $data = Array('ajaxloginstatus' => true);
            return $data;
        } else {

            $data = Array('ajaxloginstatus' => false);
            return $data;
        }


    }


}
