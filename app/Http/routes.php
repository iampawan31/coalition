<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses' => 'FormController@index',
    'as' => ' dashboard'
]);

Route::post('/addproduct', [
    'uses' => 'FormController@addProduct',
    'as' => 'addproduct'
]);

Route::get('/fetchproducts', [
    'uses' => 'FormController@fetchProducts',
    'as' => 'fetchproducts'
]);

Route::get('/fetchsingleproduct/{id}', [
    'uses' => 'FormController@fetchSingleProduct',
    'as' => 'fetchsingleproduct'
]);

Route::post('/editproduct', [
    'uses' => 'FormController@postEditProduct',
    'as' => 'editproduct'
]);