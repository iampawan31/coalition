<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //
    
    public function getCreatedAtAttribute($date)
    {

        $date = new \Carbon\Carbon($date);

        return $date->format('m-d-Y h:m a');
    }
}
