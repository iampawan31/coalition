var vm = new Vue({
    el: "#productsblock",
    data: {
        productToken: '',
        productName: '',
        productQtyInStock: '',
        pricePerItem: '',
        ajaxSignInCheckError: false,
        addProductError: '',
        products: [],
        totalProductValue: '',
        singleProductId: '',
        singleProductToken: '',
        singleProductName: '',
        singleProductQty: '',
        singleProductPrice: ''
    },
    created: function () {
        this.fetchProducts();
    },
    methods: {
        submitAddProductForm: function (event) {
            event.preventDefault();
            this.formEditInputs = {
                _token: this.productToken,
                productname: this.productName,
                productqtyinstock: this.productQtyInStock,
                priceperitem: this.pricePerItem
            };

            this.$http.post('/addproduct', this.formEditInputs, {
                    headers: {
                        'X-CSRF-TOKEN': this.editToken
                    }
                })
                .then(function (data) {
                    if (data.data.ajaxloginstatus) {
                        this.fetchProducts();
                    } else {
                        this.ajaxSignInCheckError = true;
                        this.addProductError = "Add Product Failed, Please try again";
                    }


                })
                .catch(function (data, status, request) {
                //    Check Error Later
                });

        },
        fetchProducts: function (event) {
            // console.log("Fecth Products");
            this.$http.get('/fetchproducts', {
                    headers: {
                        'X-CSRF-TOKEN': this.editToken
                    }
                })
                .then(function (data) {
                    console.log(data);
                    this.products = data.data.products;
                    this.totalProductValue = data.data.total;

                    console.log(this.products);
                });
        },
        editproduct: function (id, event) {
            event.preventDefault();
            this.singleProductId = id;
            this.$http.get('/fetchsingleproduct/' + id, {
                    headers: {
                        'X-CSRF-TOKEN': this.editToken
                    }
                })
                .then(function (data) {
                    // console.log(data);
                    this.singleProductName = data.data.product_name;
                    this.singleProductQty = data.data.quantity_in_stock;
                    this.singleProductPrice = data.data.price_per_item;
                });
            $('#editproduct').modal();
        },
        updateproduct: function (event) {
            event.preventDefault();
            this.$http.post('/editproduct', {
                    productid: this.singleProductId,
                    productname: this.singleProductName,
                    productqty: this.singleProductQty,
                    productprice: this.singleProductPrice
                }, {
                    headers: {
                        'X-CSRF-TOKEN': this.singleProductToken
                    }
                })
                .then(function (data) {
                    if (data.data.ajaxloginstatus) {
                        this.fetchProducts();
                        $('#editproduct').modal('hide');
                    } else {
                        this.ajaxSignInCheckError = true;
                        this.addProductError = "Edit Product Failed, Please try again";
                    }


                })
        }
    }


});
