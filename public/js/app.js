/**
 * Created by Warlock on 7/22/2016.
 */
var vm = new Vue({
    el: "#signup-login-block",
    data: {
        signupShow: true,
        loginShow: false,
        signUpName: '',
        signUpEmail: '',
        signUpPassword: '',
        signInEmail: '',
        signInPassword: '',
        ajaxSignInCheckError: false,
        formSignUpInputs: {},
        formSignUpErrors: {},
        formSignInInputs: {},
        formSignInErrors: {}
    },
    methods: {
        toggleLogin: function (event) {
            event.preventDefault();
            this.loginShow = true;
            this.signupShow = false;
        },
        toggleSignup: function (event) {
            event.preventDefault();
            this.loginShow = false;
            this.signupShow = true;
        },
        submitSignUpForm: function (event) {
            var form = event.target;
            var action = form.action;
            var csrfToken = form.querySelector('input[name="_token"]').value;

            this.formSignUpInputs = {
                name: this.signUpName,
                email: this.signUpEmail,
                password: this.signUpPassword
            }

            this.$http.post(action, this.formSignUpInputs, {
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    }
                })
                .then(function () {
                    form.submit();
                })
                .catch(function (data, status, request) {
                    var errors = data.data;
                    this.formSignUpErrors = errors;
                });

        },
        submitLoginForm: function (event) {
            var form1 = event.target;
            var action = form1.action;
            var csrfToken = form1.querySelector('input[name="_token"]').value;

            this.formSignInInputs = {
                email: this.signInEmail,
                password: this.signInPassword
            }

            this.$http.post(action, this.formSignInInputs, {
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    }
                })
                .then(function (data) {
                    //console.log(data.data.ajaxloginstatus);
                    if (data.data.ajaxloginstatus) {
                        form1.submit();
                    } else {
                        this.ajaxSignInCheckError = true;
                    }

                })
                .catch(function (data, status, request) {
                    var errors = data.data;
                    this.formSignInErrors = errors;
                });

        }
    }


});
