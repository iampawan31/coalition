@extends('layout.master')
@section('title')
    Dashboard | Coalition
@endsection
@section('content')

    <div class="container" id="productsblock">
        <div class="page-header">
            <h1>Product
                <small> Stock Management</small>
            </h1>
        </div>
        <div class="row">
            <p v-model="addProductError" v-if="ajaxSignInCheckError"></p>
        </div>
        <section class="row">
            <h3 class="products-list">Add Product:</h3>
            <form @submit.prevent="submitAddProductForm" action="{{route('addproduct')}}" method="post" id="addproduct">
                <input type="hidden" name="_token" value="{{ Session::token() }}" v-model="productToken">
                <div class="form-group">
                    <label for="productname">Product Name</label>
                    <input type="text" class="form-control" id="productname" name="productname" v-model="productName"
                           required>
                </div>
                <div class="form-group">
                    <label for="productqtyinstock">Qty in Stock</label>
                    <input type="text" class="form-control" id="productqtyinstock" name="productqtyinstock"
                           v-model="productQtyInStock" required>
                </div>
                <div class="form-group">
                    <label for="priceperitem">Price Per Item</label>
                    <input type="text" id="priceperitem" class="form-control" name="priceperitem" v-model="pricePerItem"
                           required>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </section>
        <section class="row">
            <h3 class="products-list">Products:</h3>
            <table class="table">
                <thead>
                <th>ID</th>
                <th>Product Name</th>
                <th>Product Quantity in Stock</th>
                <th>Price Per Item</th>
                <th>Created At</th>
                <th>Total Value</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <tr v-for="(index,product) in products">
                        <td>@{{index + 1}}</td>
                        <td>@{{product.product_name}}</td>
                        <td>@{{product.quantity_in_stock}}</td>
                        <td>@{{product.price_per_item}}</td>
                        <td>@{{product.created_at}}</td>
                        <td>@{{product.quantity_in_stock * product.price_per_item}}</td>
                        <td><a href="{{route('editproduct')}}" v-on:click="editproduct(product.id, $event)">Edit</a></td>
                    </tr>
                </tbody>
                <tfoot>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>@{{totalProductValue}}</th>
                <th></th>
                </tfoot>
            </table>
        </section>
        <div class="modal fade" tabindex="-1" role="dialog" id="editproduct">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Product</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{ Session::token() }}" v-model="singleProductToken">
                        <div class="form-group">
                            <input type="text"class="form-control" v-model="singleProductName"
                                      name="singleproductname" required>
                        </div>
                        <div class="form-group">
                            <input type="text"class="form-control" v-model="singleProductQty"
                                   name="singleproductqty" required>
                        </div>
                        <div class="form-group">
                            <input type="text"class="form-control" v-model="singleProductPrice"
                                   name="singleproductprice" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" v-on:click="updateproduct">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
@endsection